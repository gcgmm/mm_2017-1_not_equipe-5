﻿using UnityEngine;
using System.Collections;


public class Rotate : MonoBehaviour
{

    public float speed;
    public  GameObject target;
    private float ang = 0;

    void Update()
    {
        ang = speed * Time.deltaTime;
        
       transform.LookAt(target.transform.position, Vector3.left);
    }
}