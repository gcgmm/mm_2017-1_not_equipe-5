﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbita: MonoBehaviour {

    public Transform objeto;
    public int speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(objeto);
        transform.Translate(speed * Time.deltaTime, 0, 0);

	}
}
