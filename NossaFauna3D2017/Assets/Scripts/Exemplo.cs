﻿using UnityEngine;
using System.Collections;

public class Exemplo : MonoBehaviour {

   public GameObject Objeto;

   void Update () {
      if(Input.GetKeyDown("i")){
         if(Objeto.gameObject.activeSelf == true){
            Objeto.gameObject.SetActive(false);
         }
         else if(Objeto.gameObject.activeSelf == false){
            Objeto.gameObject.SetActive(true);
         }
      }
   }
}