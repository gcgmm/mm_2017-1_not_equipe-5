﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour {


    public AudioClip[] MClipsCapivara;
    public int i = 0;
    private static AudioSource audio;
    public bool playerAudio = false;
    static AudioClip[] AudioCapivara;
    //public AudioClip impact;
    //AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
       

        // audio = GetComponent<AudioSource>();
    }

    private void Update()
    {

        if (!audio.isPlaying)

            if (i < MClipsCapivara.Length && playerAudio)
            {
                {
                    audio.clip = MClipsCapivara[i];
                    audio.Play();
                    playerAudio = false;
                    i++;

                }
            }

    }
    
}
