﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimenta : MonoBehaviour {
    public float movimento = 1;
   
    
   // public float higth = 10;
    

	// Use this for initialization
	void Start () {
       // GetComponent<Animation>().Play();
        
       
        //Vector3 t =   Vector3.up * higth + transform.position;
        //  transform.position =  t;


    }
    
	
	// Update is called once per frame
	void LateUpdate () {
       /* if (!GetComponent<Animation>().isPlaying && StereoCamera.camera_orbita != false)
        {
            StereoCamera.setOrbita(false);
        }*/
       
        float movimento2 = movimento * Time.deltaTime;
        if (Input.GetKey(KeyCode.UpArrow))
            transform.position = Vector3.up * movimento2 + transform.position;
        if (Input.GetKey(KeyCode.DownArrow))
            transform.position = Vector3.down * movimento2 + transform.position;
        if (Input.GetKey(KeyCode.RightArrow))
            transform.position = Vector3.right * movimento2 + transform.position;
        if (Input.GetKey(KeyCode.LeftArrow))
            transform.position = Vector3.left * movimento2 +transform.position;
        if (Input.GetKey(KeyCode.V))
            transform.Rotate(Vector3.back * movimento2);
        if (Input.GetKey(KeyCode.B))
            transform.Rotate(Vector3.forward );

    }
}
