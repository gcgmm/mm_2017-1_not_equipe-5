﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class OrbitaManeger : MonoBehaviour {

    public float orbita_speed = 15;
    public float orbita_speedMin = 1;
    public float redutor = 10;
    public Transform target;

    
    
    public void orbitaTarget(Transform t)     
   
    {
        target = t;

        if (orbita_speed > orbita_speedMin)
        {
            orbita_speed -= Time.deltaTime / redutor;
        }
        else
        {
            orbita_speed = 0;
        }
        transform.RotateAround(target.transform.position, Vector3.up, Time.deltaTime * orbita_speed);

    }
    // Use this for initialization
    void Start () {


		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
