﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadsPrefs : MonoBehaviour {


    public string trilhaName;
    static int SceneSequence;
    public string loadingSceneName = "capybara1";
    public static GameObject trilhaComponente;
    public GameObject Capivara;
    public GameObject Tucano;
    public GameObject Tamandua;
    public GameObject CameraM;
    public GameObject PontoFlutuante;
    Animator animator;




    void Start()
    {

        Scene scene = SceneManager.GetActiveScene();
        animator = GetComponent<Animator>();


        if (scene.name.ToUpper().Equals(loadingSceneName.ToUpper()))
        {
            string trilha = getTrilhaName();
            

            switch (trilha)      {

                case "Capivara":
                    setCapivara();
                    break;
                case "Tamandua":
                    setTamandua();
                    break;
                case "Tucano":
                    setTucano();
                    break;
                default:
                    Capivara.SetActive(true);
                    trilhaName = getTrilhaName();
                    break;
                    


            }
           
          
         }
        
    }


    private void setTucano()
    {
        Tucano.SetActive(true);
        trilhaName = getTrilhaName();
        //CameraM.transform.localPosition = new Vector3(-4.1f,0.736f,16.32f);
        CameraM.transform.localPosition = new Vector3(-3.76f, 0.736f, 16.32f);
        PontoFlutuante.transform.localPosition = new Vector3(-4.16f,0.49f,20.25f);
        animator.SetInteger("Animal", 2);

    }


    private void setCapivara()
    {

        Capivara.SetActive(true);
        trilhaName = getTrilhaName();
        CameraM.transform.localPosition = new Vector3(-4.88f, 0.09f, 14.33f);
        PontoFlutuante.transform.localPosition = new Vector3(-4.95f, -1.17f, 20.33f);
        animator.SetInteger("Animal", 1);

    }
    private void setTamandua()
    {
        Tamandua.SetActive(true);
        trilhaName = getTrilhaName();
        CameraM.transform.localPosition = new Vector3(-5.13f, -0.25f, 13.23f);
        PontoFlutuante.transform.localPosition = new Vector3(-4.95f, -1.17f, 20.33f);
        animator.SetInteger("Animal", 3);

    }
    private void Update()
    {

        if (Input.GetKeyDown("escape"))
        {
            //Application.LoadLevel(Application.loadedLevel);
            SceneManager.LoadScene("Tela");
        }
      
    }

    public void setTrilhaName(string name)
    {
        trilhaName = name;
        PlayerPrefs.SetString("trilha", name);
    }

    public  string getTrilhaName(){

        string name = PlayerPrefs.GetString("trilha"); 
        return name;

      }

    public void loadScene(string levelScene)
    {

        SceneManager.LoadScene(loadingSceneName);
        //Application.LoadLevel(levelScene);
    }




}
