﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StereoCamera : MonoBehaviour {
	public int camera_mode=0;
	//public int stereo_mode=0;
	public Transform target;
    public float displacement= 6.2f;
	public bool stereo=true;
	public Camera cameraL; 
    public  bool camera_orbita = true;
    //public bool rotateControl = true;
    //public  Vector3 camera_direction;
    
    public float orbita_speed = 0;
    public float orbita_speedMin = 1;
    public float orbita_speedMax = 15;
    public float orbita_mod = 10;
    public static bool orbita_habilita =false;

    private static Vector3 positionBase;

    private Vector3 positionTroca;
    private bool atualizaPosition = false;
    public bool teste = true;

   
  


    // Use this for initialization
    void Start () {
	//	saveRot=transform.rotation;
	//    savePos=transform.position;
        positionBase = transform.position;

    }

    public void setOrbita(bool orbita)
    {
        camera_orbita = orbita;
    }
    public static void habilitaOrbita(bool orbita)
    {
        orbita_habilita = orbita;
    }

    public static void setPositionBase(Vector3 v) { 
        positionBase = v;    

    }

    void LateUpdate() {



        if (Input.GetKey("f1") && displacement > -200)
        {
            displacement -= 0.01f;
          //  rotateControl = true;
        }

        if (Input.GetKey("f2") && displacement < 200)
        {
            displacement += 0.01f;
           // rotateControl = true;
        }

        if (Input.GetKey(KeyCode.V) )
            
        {
            
            transform.RotateAround(target.transform.position, Vector3.up, orbita_mod * Time.deltaTime);

            positionBase = transform.position;
        }
        if (Input.GetKey(KeyCode.B) )
        {
            transform.RotateAround(target.transform.position, Vector3.down , orbita_mod * Time.deltaTime);

            positionBase = transform.position;
            // rotateControl = true;
        }

        if (Input.GetKey(KeyCode.N))
        {
            trocaOrbita();

        }

        if (Input.GetKey(KeyCode.Keypad1))
        {
            positionTroca = new Vector3(-4, 0, 10);




        }




        /*  if (Input.GetKeyDown("escape"))
              //Application.LoadLevel(Application.loadedLevel);
              SceneManager.;
          //SceneManager.LoadScene(SceneManager.);
          */


    

        if (Input.GetKeyUp(KeyCode.X)){
			Camera c=transform.GetComponent<Camera>();
			if(c.rect.width==0.5f)
				c.rect=new Rect(0,0,1,1);
			else
				c.rect=new Rect(0,0,0.5f,1);
		}
        /*if (Input.GetKeyUp(KeyCode.H)){
			showInfo=!showInfo;
		} */

        //  orbitaTarget();

        movimenta();

        if (atualizaPosition)
        {
            positionBase = positionTroca;
            transform.position = positionBase;
            atualizaPosition = false;
            teste = atualizaPosition;
        } 

        switch (camera_mode){
		case 0:simpleFollow();break;
		
		}




    }

    void movimenta()
    {
        float movimento2 = 1* Time.deltaTime;
        if (Input.GetKey(KeyCode.W))
        {
            transform.position = positionBase ;
            transform.position = Vector3.up * movimento2 + transform.position;
            positionBase = transform.position;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position = positionBase;
            transform.position = Vector3.down * movimento2 + transform.position;
            positionBase = transform.position;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position = positionBase;
            transform.position = Vector3.right * movimento2 + transform.position;
            positionBase = transform.position;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position = positionBase;
            transform.position = Vector3.left * movimento2 + transform.position;
            positionBase = transform.position;
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.position = positionBase;
            transform.position = Vector3.forward * movimento2 + transform.position;
            positionBase = transform.position;
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.position = positionBase;
            transform.position = Vector3.back * movimento2 + transform.position;
            positionBase = transform.position;
        }
        if (Input.GetKey(KeyCode.E))
        {
            PlayerPrefs.SetString("CameraCabeçaCapivaraX", positionBase.x.ToString());
        }
        

    }
 
    void trocaOrbita()
    {
        orbita_habilita = !orbita_habilita;
    }

    void orbita()
    {
       // teste = orbita_habilita;
        if (!orbita_habilita)            
        {
           
            if (orbita_speed > orbita_speedMin)
            {
                orbita_speed -= Time.deltaTime * orbita_mod;
            }
            else
            {
                orbita_speed = 0;
            }
        }
        else
        {            
            if (orbita_speed < orbita_speedMax)
            {
                orbita_speed += Time.deltaTime * orbita_mod;
            }
            else
            {
                orbita_speed = orbita_speedMax;
            }


        }
    }

    void simpleFollow() {


        transform.LookAt(target);
        transform.position = positionBase;

        if (camera_orbita)
        {
            
            orbita();
           
            transform.RotateAround(target.transform.position, Vector3.up,  Time.deltaTime * orbita_speed);
			
			positionBase = transform.position;
        }

        //transform.position =  target.position;
        //transform.position -= Vector3.forward * distance;       
        //Vector3 h=transform.position; 
        //h.y+=height;
        //transform.position= h;

        transform.position = positionBase;

        

       if (stereo)
        {
            cameraL.transform.position = transform.position;
            cameraL.transform.rotation = transform.rotation;

            Camera.main.transform.RotateAround(target.position, Vector3.up, -displacement * 2);   
            cameraL.transform.RotateAround(target.position, Vector3.up, displacement * 2);
            

        }
        
		
	}

    public void  setPositionCam(Vector3 v3)    {

        positionTroca =  v3;
        atualizaPosition = true;    

    }

}

